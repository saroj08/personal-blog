
<?php
  include 'layout.php';
?>

  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Start Blog</h1>
            <span class="subheading">More Blogs</span>
          </div>
        </div>
      </div>
    </div>
  </header>

  <?php
        include 'connection/connection.php';
         $sql = "SELECT * FROM blog ";
          $result = $conn->query($sql);
            if($result->num_rows>0) {
                while($row = $result->fetch_assoc()) {
                      $title = $row['title'];
                      $substring = substr($row['contant'],0,80); 
                      $date = $row['reg_date'];
                      $id = $row['id'];

         ?>
  <!-- Main Content -->
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <div class="post-preview">
        <?php
            echo   "<a href='post.php/?post=$id'>"
          ?>
            <h2 class="post-title">
              <?php echo $title;?>
            </h2>
            <h3 class="post-subtitle">
              <?php
                echo strip_tags($substring)."...";
              ?>
            </h3>
          </a>
          <p class="post-meta">Posted on
            <a href="#"></a>
            <?php echo $date; ?></p>
        </div>
        <hr>
      </div>
    </div>
  </div>
  <?php
      }
    }
  ?>

  <hr>

  <!-- Footer -->
  <footer>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <ul class="list-inline text-center">
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
            <li class="list-inline-item">
              <a href="#">
                <span class="fa-stack fa-lg">
                  <i class="fas fa-circle fa-stack-2x"></i>
                  <i class="fab fa-github fa-stack-1x fa-inverse"></i>
                </span>
              </a>
            </li>
          </ul>
          <p class="copyright text-muted">Copyright &copy; Your Website 2019</p>
        </div>
      </div>
    </div>
  </footer>

