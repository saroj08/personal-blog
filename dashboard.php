<?php
    include 'layout.php';
?>
  <!-- Page Header -->
  <header class="masthead" style="background-image: url('img/home-bg.jpg')">
    <div class="overlay"></div>
    <div class="container">
      <div class="row">
        <div class="col-lg-8 col-md-10 mx-auto">
          <div class="site-heading">
            <h1>Dashboard Table</h1>
            <span class="subheading">More Blogs</span>
          </div>
        </div>
      </div>
    </div>
  </header>


  <table class="table  table-bordered container">
  <thead>
    <tr>
      <th scope="col">id</th>
      <th scope="col">Name</th>
      <th scope="col">Title</th>
      <th scope="col">Edit</th>
      <th scope="col">Delete</th>
    </tr>
  </thead>
  <tbody>
  <?php
        include 'connection/connection.php';
         $sql = "SELECT * FROM blog ";
          $result = $conn->query($sql);
            if($result->num_rows>0) {
                while($row = $result->fetch_assoc()) {
                      $name = $row['name'];
                      $title = $row['title'];
                      $id = $row['id'];

         ?>
           <form action="edit-query.php"  method="POST" >
                <th scope="row"><?php echo $id; ?></th>
                <td><?php echo $name; ?></td>
                <td><?php echo $title; ?></td>
                <input type="hidden" name = "id" value = "<?php echo $id; ?>"/>
                <td><input type="submit" value="Edit" > </td>
              </form>
              <form action="delete-query.php"  method="POST" >
                <input type="hidden" name = "id" value = "<?php echo $id; ?>"/>
                <td><input type="submit" value="Delete" > </td>
              </form>
            
  </tbody>
  <?php
         }

        }
    ?>

</table>



  <hr>
<!-- Footer -->
<footer>
  <div class="container">
    <div class="row">
      <div class="col-lg-8 col-md-10 mx-auto">
        <ul class="list-inline text-center">
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-twitter fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-facebook-f fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
          <li class="list-inline-item">
            <a href="#">
              <span class="fa-stack fa-lg">
                <i class="fas fa-circle fa-stack-2x"></i>
                <i class="fab fa-github fa-stack-1x fa-inverse"></i>
              </span>
            </a>
          </li>
        </ul>
        <p class="copyright text-muted">Copyright &copy; 2019</p>
      </div>
    </div>
  </div>
</footer>
