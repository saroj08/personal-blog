<?php
    include '../connection/connection.php';

    $admin = "CREATE TABLE admin(
        id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        name varchar(50),
        password varchar(50)
     )";
    
    if ($conn->query($admin) === TRUE) {
        echo "admin table create successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }
?>
