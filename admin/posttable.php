<?php
    include '../connection/connection.php';

    $sql = "CREATE TABLE blog(
        id INT(11) PRIMARY KEY NOT NULL AUTO_INCREMENT,
        name varchar(50),
        title varchar(50) not null,
        contant varchar(1000) not null,
        reg_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
     )";
    
    if ($conn->query($sql) === TRUE) {
        echo "blog   table create successfully";
    } else {
        echo "Error creating table: " . $conn->error;
    }
?>
